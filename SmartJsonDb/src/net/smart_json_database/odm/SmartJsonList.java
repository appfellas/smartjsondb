package net.smart_json_database.odm;

import java.util.ArrayList;


public class SmartJsonList<T extends IJSONDocument> extends ArrayList<T> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6435645250626025999L;

	public T first() {
		if (size() > 0) {
			return get(0);
		}
		return null;
	}

	public int count() {
		return size();
	}

	// TODO Finish sorting, probably in a bit different way
	// public void sortBy(String fieldName, String order) {
	// for (T item : this) {
	// Field fld = item.getField(fieldName);
	// if (fld != null) {
	// try {
	// Object value = fld.get(item);
	// // XXX Does nothing
	// } catch (IllegalArgumentException e) {
	// e.printStackTrace();
	// } catch (IllegalAccessException e) {
	// e.printStackTrace();
	// }
	// }
	// }
	// }

}
