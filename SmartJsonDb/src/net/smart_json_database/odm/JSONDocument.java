package net.smart_json_database.odm;

import java.lang.reflect.Field;

import net.smart_json_database.odm.annotations.IgnoreField;
import net.smart_json_database.odm.exceptions.ModelIdIsNotDefinedException;
import android.util.Log;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class JSONDocument implements IJSONDocument {

	@IgnoreField
	private static final String TAG = "JSONDocument";

	@IgnoreField
	@JsonIgnore
	private Repository<?> repository;

	/**
	 * Id of this entity in SQL
	 */
	public int _superId = -1;

	public int get_superId() {
		return _superId;
	}

	public void set_superId(int _superId) {
		this._superId = _superId;
	}

	public boolean save() throws ModelIdIsNotDefinedException {

		if (getRepository() == null) {
			Log.d(TAG, "Repo is null - cannot save");
			return false;
		}
		return this.getRepository().save(this);
	}
	
	

	public Repository<?> getRepository() {
		return repository;
	}

	public void setRepository(Repository<?> repository) {
		this.repository = repository;
	}

	@Override
	public Field getField(String name) {

		try {
			Field fld = getClass().getField(name);
			return fld;
		} catch (NoSuchFieldException e) {

			e.printStackTrace();
			return null;
		}

	}

	@Override
	public String toString() {

		String output = this.getClass().getName() + " // ";

		Field[] fields = this.getClass().getDeclaredFields();
		for (Field field : fields) {

			field.setAccessible(true);
			try {

				IgnoreField anno = field.getAnnotation(IgnoreField.class);

				if (anno == null) {
					// Log.d(TAG,
					// "Save :" + field.getName() + ": " + field.get(this));
					output += "| " + field.getName() + " : " + field.get(this)
							+ " \n";
				}
			} catch (IllegalArgumentException e) {

				e.printStackTrace();
			} catch (IllegalAccessException e) {

				e.printStackTrace();
			}
		}

		output += "// end of " + this.getClass().getName() + " \n";

		return output;

	}
	
	

//	@Override
//	public void initializePoolObject() {
//
//		Field[] fields = this.getClass().getDeclaredFields();
//		for (Field field : fields) {
//			if (field.getAnnotation(IgnoreField.class) == null) {
//				try {
//					if (field.getType().equals(String.class)) {
//						field.set(this, "");
//					} else if (field.getType().equals(int.class)) {
//						field.set(this, 0);
//					} else if (field.getType().equals(boolean.class)) {
//						field.set(this, false);
//					} else if (field.getType().equals(double.class)) {
//						field.set(this, 0.0);
//					} else if (field.getType().equals(float.class)) {
//						field.set(this, 0.0f);
//					}
//				} catch (IllegalArgumentException e) {
//					e.printStackTrace();
//				} catch (IllegalAccessException e) {
//					e.printStackTrace();
//				}
//			}
//		}
//
//		set_superId(-1);
//		setRepository(null);
//	}

//	@Override
//	public void finalizePoolObject() {
//
//	}

}
