package net.smart_json_database.odm;

import java.lang.reflect.Field;

import net.smart_json_database.odm.exceptions.ModelIdIsNotDefinedException;




public interface IJSONDocument {

	public int get_superId();

	public void set_superId(int _superId);

	public boolean save() throws ModelIdIsNotDefinedException;

	public Repository<?> getRepository();

	public void setRepository(Repository<?> repository);
	
	public Field getField(String name);
	

}
