package net.smart_json_database.odm;

public class ModelConversionException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8677963706391901545L;

	public ModelConversionException() {
		super();
	}
	
	public ModelConversionException(String msg) {
		super(msg);
	}
}
