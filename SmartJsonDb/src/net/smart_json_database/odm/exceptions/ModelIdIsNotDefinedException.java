package net.smart_json_database.odm.exceptions;

public class ModelIdIsNotDefinedException extends Exception {

	public ModelIdIsNotDefinedException(String msg) {
		super(msg);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 936579346872902173L;

}
