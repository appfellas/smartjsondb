package net.smart_json_database.odm;

public class CollectionExpectedException extends RuntimeException {

	public CollectionExpectedException(String msg) {
		super(msg);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 5478377592338018888L;

}
