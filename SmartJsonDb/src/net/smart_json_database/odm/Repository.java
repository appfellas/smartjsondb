package net.smart_json_database.odm;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import net.smart_json_database.InitJSONDatabaseException;
import net.smart_json_database.JSONDatabase;
import net.smart_json_database.JSONEntity;
import net.smart_json_database.SearchFields;
import net.smart_json_database.odm.annotations.BelongsTo;
import net.smart_json_database.odm.annotations.HasAndBelongsToMany;
import net.smart_json_database.odm.annotations.HasMany;
import net.smart_json_database.odm.annotations.Id;
import net.smart_json_database.odm.annotations.IgnoreField;
import net.smart_json_database.odm.exceptions.ModelIdIsNotDefinedException;

import org.json.JSONException;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

public class Repository<T extends IJSONDocument> {
	private static final String TAG = "Repository";
	public static final String ASC = "asc";
	public static final String DESC = "desc";
	@IgnoreField
	private static JSONDatabase db;
	@IgnoreField
	public Context context;

	protected T modelType;

	@IgnoreField
	protected boolean _saved;

	// private T model;
	private JSONEntity reusableEntity = null;
	private Class<T> modelClass;
	// private SmartJsonList<T> reusableCollection;
	private Field modelIdField;
	private Field[] modelFields;

	// private HashMap<Class<? extends JSONDocument>, String> relManyToMany;
	// private HashMap<Class<? extends JSONDocument>, String> relBelongsTo;

	public Repository(Class<T> modelClass, Context context) {
		this.modelClass = modelClass;
		this.context = context;
		cacheClassMetaData(modelClass);
	}

	private void cacheClassMetaData(Class<T> clazz) {

		// relManyToMany = new HashMap<Class<? extends JSONDocument>, String>();
		// relBelongsTo = new HashMap<Class<? extends JSONDocument>, String>();

		// Annotation[] annos = clazz.getAnnotations();
		// for (Annotation annotation : annos) {
		// Log.d(TAG, "Found annotation: "
		// + annotation.annotationType().getName());
		// if (annotation.annotationType().getName()
		// .equals(HasAndBelongsToMany.class.getName())) {
		// HasAndBelongsToMany an = (HasAndBelongsToMany) annotation;
		// Log.d(TAG, "This class has ManyToMany Relation with :"
		// + an.value().getName());
		// if (!relManyToMany.containsKey(an.value())) {
		// relManyToMany.put(an.value(), an.on());
		// }
		// }

		// if (annotation.annotationType().getName()
		// .equals(BelongsTo.class.getName())) {
		// BelongsTo an = (BelongsTo) annotation;
		// Log.d(TAG, "This class belongs to: " + an.value().getName());
		//
		// if (!relBelongsTo.containsKey(an.value())) {
		// relBelongsTo.put(an.value(), field.getN);
		// }
		// }
		// }

		Field[] fields = clazz.getDeclaredFields();

		ArrayList<Field> fldz = new ArrayList<Field>();
		for (Field field : fields) {
			field.setAccessible(true);
			if (field.getAnnotation(IgnoreField.class) == null) {
				fldz.add(field);
			}
			if (field.getAnnotation(Id.class) != null) {
				modelIdField = field;
			}

		}

		modelFields = new Field[fldz.size()];
		fldz.toArray(modelFields);
	}

	public SmartJsonList<T> entitiesToModels(Collection<JSONEntity> entities)
			throws InstantiationException, IllegalAccessException {
		SmartJsonList<T> сollection = new SmartJsonList<T>();

		for (JSONEntity entity : entities) {
			T m = makeFromEntity(entity);
			m.set_superId(entity.getUid());
			if (m != null) {
				сollection.add(m);
			}
		}
		return сollection;
	}

	public IJSONDocument makeFromEntityForType(
			Class<? extends IJSONDocument> clazz, JSONEntity entity)
			throws IllegalAccessException, InstantiationException {

		IJSONDocument pl = clazz.newInstance();

		//pl.setRepository(this);
		pl.set_superId(entity.getUid());

		// XXX It is importatnt to use field from a new entity
		for (Field field : clazz.getDeclaredFields()) {
			field.setAccessible(true);
			try {
				// Relations
				if (field.getAnnotation(BelongsTo.class) != null) {
					// This object belongs to another object
					// This is only one way now

					BelongsTo an = field.getAnnotation(BelongsTo.class);
					// Log.d(TAG, "It belongs to a class: " +
					// an.value().getName());
					if (entity.containsBelongsToRelation(field.getName())) {
						ArrayList<Integer> rels = (ArrayList<Integer>) entity
								.belongsTo(field.getName());

						if (rels.size() > 0) {
							Integer id = rels.get(0);
							// Log.d(TAG, "id: " + id + " type: "
							// + field.getType().getName());
							Object relationObj = getDb().fetchById(id);

							if (relationObj != null) {
								Object relationModel = makeFromEntityForType(
										an.value(), (JSONEntity) relationObj);
								// Log.d(TAG, "This object has relation to "
								// + field.getName() + " of class "
								// + an.value().getName());
								field.set(pl, an.value().cast(relationModel));
								// Log.d(TAG, field.get(pl).toString());
							} else {
								Log.e(TAG, "Relation object is null");
							}
						}
					}

				} else

				if (field.getAnnotation(HasAndBelongsToMany.class) != null) {
					HasAndBelongsToMany an = field
							.getAnnotation(HasAndBelongsToMany.class);
					
					Log.d(TAG, "Has many and belongs to many: "+ field.getName());
					Log.d(TAG, "Entity: "+entity.toString());
					Collection<Integer> relations = entity.hasMany(field.getName());
					
					ArrayList<Object> models = new ArrayList<Object>();
					
					for (Integer entityId : relations) {
						Log.d(TAG, "Entity ID: "+entityId);
						JSONEntity relationObj = getDb().fetchById(entityId);
						
						if (relationObj != null) {
							IJSONDocument relationModel = makeFromEntityForType(
									an.value(), relationObj);
							
							models.add(relationModel);
						} else {
							Log.e(TAG, "Relation object is null");
						}
					}
					
					Log.d(TAG, "Got "+models.size()+" related objects");
					Log.d(TAG, "Type: "+field.getType().getName());

					field.set(pl, field.getType().cast(models));

				} else
				

				if (field.getAnnotation(HasMany.class) != null) {
					HasMany an = field.getAnnotation(HasMany.class);
					Log.d(TAG, "It has many: " + an.value().getName());
				}

				// Values
				if (entity.has(field.getName())) {

					if (field.getType().equals(Integer.class)
							&& ((Object) entity.getInt(field.getName())) != null) {
						field.set(pl, (Integer) entity.getInt(field.getName()));
					} else

					if (field.getType().equals(int.class)
							&& ((Object) entity.getInt(field.getName())) != null) {
						field.set(pl, entity.getInt(field.getName()));
					} else

					if (field.getType().equals(String.class)
							&& entity.getString(field.getName()) != null) {
						field.set(pl, entity.getString(field.getName()));
					} else

					if (field.getType().equals(Boolean.class)
							&& (Object) entity.getBoolean(field.getName()) != null) {
						field.set(pl,
								(Boolean) entity.getBoolean(field.getName()));
					} else

					if (field.getType().equals(boolean.class)
							&& (Object) entity.getBoolean(field.getName()) != null) {
						field.set(pl, entity.getBoolean(field.getName()));
					} else

					if (field.getType().equals(Double.class)
							&& (Double) entity.getDouble(field.getName()) != null) {
						field.set(pl,
								(Double) entity.getDouble(field.getName()));
					} else

					if (field.getType().equals(double.class)
							&& (Double) entity.getDouble(field.getName()) != null) {
						field.set(pl, entity.getDouble(field.getName()));
					} else

					if (field.getType().equals(float.class)
							&& (Float) (float) entity
									.getDouble(field.getName()) != null) {
						field.set(pl, (float) entity.getDouble(field.getName()));
					} else

					if (field.getType().equals(Float.class)
							&& (Float) (float) entity
									.getDouble(field.getName()) != null) {
						field.set(pl, (Float) (float) entity.getDouble(field
								.getName()));
					} else

					if (field.getType().equals(Long.class)
							&& (Long) entity.getLong(field.getName()) != null) {
						field.set(pl, (Long) entity.getLong(field.getName()));
					} else

					if (field.getType().equals(long.class)
							&& (Long) entity.getLong(field.getName()) != null) {
						field.set(pl, entity.getLong(field.getName()));
					} else

					if (field.getType().equals(Short.class)
							&& (Short) (short) entity.getInt(field.getName()) != null) {
						field.set(pl,
								(Short) (short) entity.getInt(field.getName()));
					} else

					if (field.getType().equals(short.class)
							&& (Short) (short) entity.getInt(field.getName()) != null) {
						field.set(pl, (short) entity.getInt(field.getName()));
					}
					// TODO Add support for field retrieval
					// else if ()

				}

			} catch (IllegalArgumentException e) {
				Log.d(TAG,
						"Error while processing annotation: " + e.getMessage());
				e.printStackTrace();
			} catch (JSONException e) {
				Log.d(TAG,
						"Error while processing annotation: " + e.getMessage());
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				Log.d(TAG,
						"Error while processing annotation: " + e.getMessage());
				e.printStackTrace();
			}

		}

		return pl;
	}

	@SuppressWarnings("unchecked")
	public T makeFromEntity(final JSONEntity entity)
			throws InstantiationException, IllegalAccessException {

		return (T) makeFromEntityForType(modelClass, entity);
	}

	public Field getField(Class<?> clazz, String fieldName)
			throws NoSuchFieldException {

		try {
			return clazz.getDeclaredField(fieldName);
		} catch (NoSuchFieldException e) {
			Class<?> superClass = clazz.getSuperclass();
			if (superClass == null) {
				throw e;
			} else {
				return getField(superClass, fieldName);
			}
		}
	}

	public T create() {
		// Log.d(TAG, "Starting Create");
		try {
			setDb(JSONDatabase.GetDatabase(context));
		} catch (InitJSONDatabaseException e1) {
			e1.printStackTrace();
		}

		if (reusableEntity == null) {
			reusableEntity = new JSONEntity(modelClass.getName());
		}
		reusableEntity.reset();
		reusableEntity.setType(modelClass.getName());

		T playlist = null;

		try {
			playlist = modelClass.newInstance();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}//

		playlist.set_superId(reusableEntity.getUid());
		playlist.setRepository(this);

		return playlist;
	}

	// public boolean save() {
	// // Log.d(TAG, "*Starting save*");
	// try {
	// db = JSONDatabase.GetDatabase(context);
	// } catch (InitJSONDatabaseException e) {
	// Log.e(TAG, "DB Error: " + e.getMessage());
	// e.printStackTrace();
	// }
	//
	// // TODO throw error if Id is not specified
	//
	// JSONEntity entity = this._toJSONEntity(model);
	// Log.d(TAG, "Entity to save: " + entity.toString());
	// int res = db.store(entity);
	// this.model.set_superId(res);
	// // this.model.setRepository(this);
	//
	// if (res != JSONDatabase.OPERATION_STATUS_ERROR) {
	// return true;
	// }
	//
	// return false;
	// }

	public JSONEntity _toJSONEntity(final IJSONDocument doc) {

		JSONEntity entity = new JSONEntity(modelClass.getName());
		entity.setUid(doc.get_superId());
		// Log.d(TAG, "After entity set uid");
		// Check if entity with this ID is present in DB
		T item = null;
		try {
			item = find(modelIdField.getInt(doc));
		} catch (IllegalArgumentException e1) {
			e1.printStackTrace();
		} catch (IllegalAccessException e1) {
			e1.printStackTrace();
		}

		if (item != null) {
			entity.setUid(item.get_superId());
		}

		for (Field field : modelFields) {

			try {
				if (field.get(doc) != null) {
					// Log.d(TAG, "Field Type: " + field.getType().getName());
					// One model
					// TODO Field should be retrieved based on the annotation
					// "on" value
					// if (relBelongsTo.containsKey(field.getType())) {
					// Log.d(TAG, "This field belongs to : "
					// + field.getType().getName());
					//
					// String relFieldName = relBelongsTo.get(field.getType());
					// JSONEntity mEntity = jsonEntityForType(field.getType(),
					// field.get(doc));// addBelongsToObject(field.get(pl));
					// entity.addBelongsToObject(relFieldName, mEntity);
					//
					// } else
					//
					// if (isFoundInCollection(field.getType(), relManyToMany
					// .getClass().getTypeParameters())) {
					// Log.d(TAG, "This class has ManyToMany Relation with");
					// }

					entity.put(field.getName(), field.get(doc));

					if (field.getAnnotation(Id.class) != null) {
						entity.setIdFieldName(field.getName());
					}

				}
			} catch (IllegalArgumentException e) {
				Log.e(TAG,
						"Error while converting to JSONEntity: "
								+ e.getMessage());
				e.printStackTrace();
			} catch (JSONException e) {
				Log.e(TAG,
						"Error while converting to JSONEntity: "
								+ e.getMessage());
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				Log.e(TAG,
						"Error while converting to JSONEntity: "
								+ e.getMessage());
				e.printStackTrace();
			}
		}

		return entity;
	}

	// private boolean isFoundInCollection(Class<?> type,
	// TypeVariable<?>[] typeParameters) {
	//
	// for (TypeVariable<?> typeVariable : typeParameters) {
	// if (typeVariable.getClass().getName().equals(type.getName())) {
	// return true;
	// }
	// }
	//
	// return false;
	// }

	private JSONEntity jsonEntityForType(Class<? extends IJSONDocument> type,
			Object object) {
		if (type == null) {
			Log.e(TAG, "For some reasons type is null");
			return null;
		}

		if (object == null) {
			Log.e(TAG, "For some reasons object is null");
			return null;
		}

		JSONDocument mod = (JSONDocument) type.cast(object);
		if (mod == null) {
			Log.e(TAG, "For some reasons mod is null: type: " + type.getName());
		}

		JSONEntity entity = new JSONEntity(mod.getClass().getName());

		if (mod.get_superId() == -1) {
			Field idField = getIdField(type);
			try {
				Object id = idField.get(mod);
				Log.d(TAG, "Id found: " + id);
				// XXX one more int
				IJSONDocument res = type.cast(findObjectForType((Integer) id,
						type));
				if (res != null) {
					// IJSONDocument obj = type.cast(findObjectForType((Integer)
					// id, type));
					Log.d(TAG, "Entity found: " + res.get_superId());
					entity.setUid(res.get_superId());
				}

			} catch (IllegalArgumentException e) {
				Log.e(TAG, "Error whilte setting UID: " + e.getMessage());
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				Log.e(TAG, "Error whilte setting UID: " + e.getMessage());
				e.printStackTrace();
			}

		} else {
			entity.setUid(mod.get_superId());
		}

		try {
			Field[] fields = mod.getClass().getDeclaredFields();
			for (Field field : fields) {
				field.setAccessible(true);

				if (field.getAnnotation(BelongsTo.class) != null) {
					// This object belongs to another object
					// This is only one way now

					BelongsTo an = field.getAnnotation(BelongsTo.class);
					JSONEntity subentity = jsonEntityForType(an.value(),
							field.get(mod));
					// We need to save this subentity
					if (subentity == null) {
						Log.e(TAG, "Subentity is null - nothing to do: "
								+ field.getName());
						continue;
					}

					int id = getDb().store(subentity);
					subentity.setUid(id);
					entity.addBelongsToObject(field.getName(), subentity);
				} else

				if (field.getAnnotation(HasAndBelongsToMany.class) != null) {
					HasAndBelongsToMany an = field
							.getAnnotation(HasAndBelongsToMany.class);
					Log.d(TAG, "It has many and belongs to many: "
							+ an.value().getName());

					// Check if this is a collection
					if (!Collection.class.isAssignableFrom(field.getType())) {
						Log.d(TAG,
								"This is not a collection in hasMany* relation");
						throw new CollectionExpectedException("Field "
								+ field.getName()
								+ " has to be a collection. Got "
								+ field.getType().getName());
					} else {
						Log.d(TAG,
								"Everything is fine - we've got a collection");
					}
					// Insert/Update each item in the collection
					@SuppressWarnings("unchecked")
					Collection<? extends JSONDocument> collection = Collection.class
							.cast(field.get(mod));
					if (collection == null) {
						Log.e(TAG, "Collection is null - do not continue");
						continue;
					}
					ArrayList<JSONEntity> batch = new ArrayList<JSONEntity>();
					for (Object item : collection) {
						Log.d(TAG, "Going through collection: "
								+ item.getClass().getName());

						@SuppressWarnings("unchecked")
						JSONEntity relEntity = jsonEntityForType(
								(Class<? extends IJSONDocument>) item
										.getClass(),
								item);
						
						if (relEntity != null) {
							Log.d(TAG, "Added entity to batch: "+relEntity.getType()+ " UID: "+relEntity.getUid());
							batch.add(relEntity);
						} else {
							Log.e(TAG, "Entity was null : don't add it");
						}
					}

					ArrayList<Integer> ids = db.storeAll(batch);
					Log.d(TAG, "Stored "+ids.size()+" related items: "+field.getName()+" "+an.value().getName());
					for (Integer entityId : ids) {
						entity.addIdToHasMany(field.getName(), entityId);
					}

				} else

				if (field.getAnnotation(HasMany.class) != null) {
					HasMany an = field.getAnnotation(HasMany.class);
					Log.d(TAG, "It has many: " + an.value().getName());
				} else

				if (field.get(mod) != null) {
					entity.put(field.getName(), field.get(mod));
				}

				if (field.getAnnotation(Id.class) != null) {
					entity.setIdFieldName(field.getName());
				}
			}
		} catch (IllegalArgumentException e) {
			Log.e(TAG,
					"Error while converting to JSONEntity: " + e.getMessage());
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			Log.e(TAG,
					"Error while converting to JSONEntity: " + e.getMessage());
			e.printStackTrace();
		} catch (JSONException e) {
			Log.e(TAG,
					"Error while converting to JSONEntity: " + e.getMessage());
			e.printStackTrace();
		}

		Log.d(TAG, "Produced entity: " + entity.toString());

		return entity;
	}

	public SmartJsonList<T> all() {
		// Log.d(TAG, "Getting all records");
		try {
			setDb(JSONDatabase.GetDatabase(context));

			Collection<JSONEntity> entities = getDb().fetchByType(
					modelClass.getName());
			Log.d(TAG, "Got entities: " + entities.size());
			SmartJsonList<T> models = entitiesToModels(entities);

			return models;
		} catch (InitJSONDatabaseException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}

		return new SmartJsonList<T>();

	}

	public SmartJsonList<T> where(SearchFields fields) {
		// Log.d(TAG, "Starting where");

		try {
			setDb(JSONDatabase.GetDatabase(context));
			fields.AndFilterByType(modelClass.getName());
			Collection<JSONEntity> entities = getDb().fetchByFields(fields);
			// .And("type", Playlist.class.getClass().getName()
			SmartJsonList<T> models = entitiesToModels(entities);

			return models;
		} catch (InitJSONDatabaseException e) {
			Log.e(TAG, "Error while searching by predicate: " + e.getMessage());
			e.printStackTrace();
		} catch (InstantiationException e) {
			Log.e(TAG, "Error while searching by predicate: " + e.getMessage());
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			Log.e(TAG, "Error while searching by predicate: " + e.getMessage());
			e.printStackTrace();
		}

		return new SmartJsonList<T>();
	}

	public boolean save(final IJSONDocument doc)
			throws ModelIdIsNotDefinedException {
		// Log.d(TAG, "*Starting save*");

		try {
			// XXX Again we lock in on int. What if id is not int?
			if (modelIdField.getInt(doc) == 0) {
				Log.d(TAG, "No id found");
				throw new ModelIdIsNotDefinedException(
						"Model has not value for Id field");
			}

		} catch (IllegalArgumentException e1) {
			Log.e(TAG,
					"Error while checking for Id field presence: "
							+ e1.getMessage());
			e1.printStackTrace();
		} catch (IllegalAccessException e1) {
			Log.e(TAG,
					"Error while checking for Id field presence: "
							+ e1.getMessage());
			e1.printStackTrace();
		}

		try {
			setDb(JSONDatabase.GetDatabase(context));
		} catch (InitJSONDatabaseException e) {
			Log.e(TAG, "DB Error: " + e.getMessage());
			e.printStackTrace();
		}
		JSONEntity entity = jsonEntityForType(doc.getClass(), doc);
		Log.d(TAG, "Entity to save: " + entity.toString());
		int res = getDb().store(entity);
		Log.d(TAG, "Saved item: " + res);
		doc.set_superId(res);
		doc.setRepository(this);

		if (res != JSONDatabase.OPERATION_STATUS_ERROR) {
			return true;
		}

		return false;
	}

	@SuppressWarnings("unchecked")
	public T find(int id) {

		return (T) findObjectForType(id, modelClass);

	}

	public Object findObjectForType(int id, Class<?> clazz) {
		Log.d(TAG, "Find object for type: " + id + ": " + clazz.getName());
		try {
			db = JSONDatabase.GetDatabase(context);

			Field idField = getIdField(clazz);
			if (idField == null) {
				Log.e(TAG, "Id Field is empty.");
				return null;
			}
			String name = idField.getName();

			SearchFields query = SearchFields.FilterByType(clazz.getName())
					.And(name, id);

			Log.d(TAG, "Query: " + query.toString());

			Collection<JSONEntity> entities = getDb().fetchByFields(query);
			Log.d(TAG, "Entities found: " + entities.size());

			ArrayList<?> res = entitiesToModels(entities);
			if (res.size() > 0) {
				return res.get(0);
			}

			return null;

		} catch (InitJSONDatabaseException e) {
			Log.e(TAG, "Error while trying to find item: " + e.getMessage());
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			Log.e(TAG, "Error while trying to find item: " + e.getMessage());
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			Log.e(TAG, "Error while trying to find item: " + e.getMessage());
			e.printStackTrace();
		} catch (InstantiationException e) {
			Log.e(TAG, "Error while trying to find item: " + e.getMessage());
			e.printStackTrace();
		}

		return null;
	}

	private Field getIdField(Class<?> clazz) {

		Field[] fields = clazz.getDeclaredFields();
		for (Field field : fields) {
			field.setAccessible(true);
			if (field.getAnnotation(Id.class) != null) {
				return field;
			}
		}

		return null;
	}

	/**
	 * 
	 * @param batch
	 * @param updateStrategy
	 *            Only {@link JSONDatabase}.STRATEGY_UPDATE for now
	 * @return
	 */
	public ArrayList<Integer> saveAll(final Collection<T> batch,
			final int updateStrategy) {
		try {
			setDb(JSONDatabase.GetDatabase(context));
		} catch (InitJSONDatabaseException e) {
			Log.d(TAG, "Error while getting DB: " + e.getMessage());
			e.printStackTrace();
			return null;
		}

		List<JSONEntity> entities = new ArrayList<JSONEntity>();

		for (T item : batch) {
			JSONEntity entity = jsonEntityForType(modelClass, item);
			Log.d(TAG, "Entity to save: " + entity.toString());
			entities.add(entity);
		}
		Log.d(TAG, "After All Entities are set");
		return getDb().storeAll(entities);
	}

	public ArrayList<Integer> saveAll(T[] playlists) {
		try {
			setDb(JSONDatabase.GetDatabase(context));
		} catch (InitJSONDatabaseException e) {
			Log.d(TAG, "Error while getting DB: " + e.getMessage());
			e.printStackTrace();
			return null;
		}

		List<JSONEntity> entities = new ArrayList<JSONEntity>();

		for (T item : playlists) {
			JSONEntity entity = jsonEntityForType(modelClass, item);
			entities.add(entity);
		}
		Log.d(TAG, "After All Entities are set");
		return getDb().storeAll(entities);
	}

	public int count(SearchFields query) {
		try {
			setDb(JSONDatabase.GetDatabase(context));
		} catch (InitJSONDatabaseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		int count = getDb().count(query);

		return count;
	}

	public Cursor getCursorForQuery(SearchFields query) {

		Cursor cursor = getDb().getCursorForQuery(query);
		if (cursor.getCount() > 0) {
			cursor.moveToFirst();
		}

		return cursor;
	}

	public JSONDatabase getDb() {
		if (db == null) {
			try {
				db = JSONDatabase.GetDatabase(context);
			} catch (InitJSONDatabaseException e) {
				Log.d(TAG, "Error while getting DB: " + e.getMessage());
				e.printStackTrace();
			}
		}

		return db;
	}

	public void setDb(JSONDatabase db) {
		Repository.db = db;
	}

}
