package net.smart_json_database.odm.adapters;

import net.smart_json_database.JSONEntity;
import net.smart_json_database.SearchFields;
import net.smart_json_database.odm.IJSONDocument;
import net.smart_json_database.odm.Repository;
import net.smart_json_database.tools.Util;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public abstract class SmartJsonCursorAdapter<T extends IJSONDocument> extends
		BaseAdapter {

	private static final String TAG = "SmartJsonCursorAdapter";
	private static final int DEFAULT_MODEL_CACHE_MAX_SIZE = 100;
	// After reaching this stage it will start deleting the start of the queue
	private int modelCacheMaxSize = DEFAULT_MODEL_CACHE_MAX_SIZE;
	protected Context context;
	protected Repository<T> repo;
	protected SearchFields query;
	protected Cursor cursor;
	private SparseArray<T> _modelsCache = new SparseArray<T>(modelCacheMaxSize);
	private boolean limitModelCacheSize = false;
	private boolean cacheModels = true;

	public SmartJsonCursorAdapter(Context context, Class<T> clazz) {
		this.context = context;
		this.repo = new Repository<T>(clazz, context);
		this.query = SearchFields.FilterByType(clazz.getName());
	}

	@Override
	public int getCount() {
		return this.repo.count(query);
	}

	@Override
	public T getItem(int position) {
		if (cursor == null) {
			cursor = this.repo.getCursorForQuery(query);
		}

		if (limitModelCacheSize == true
				&& _modelsCache.size() >= getModelCacheMaxSize()) {

			int direction = position - cursor.getPosition();
			if (direction < 0) { // Going up - remove the last element in the
									// array
				_modelsCache
						.remove(_modelsCache.keyAt(_modelsCache.size() - 1));
			} else { // Going down - remove the first element in array
				_modelsCache.remove(_modelsCache.keyAt(0));
			}

			Log.d(TAG, "Cache size: " + _modelsCache.size());
		}

		if (cacheModels == true && _modelsCache.get(position) != null) {
			Log.d(TAG, "Delivered from cache: " + position);
			return (T) _modelsCache.get(position);
		}

		JSONEntity entity = getEntityFromCursor(cursor, position);
		Log.d(TAG, "Got entity from cursor: "+entity.toString());
		if (entity != null) {
			try {
				@SuppressWarnings("unchecked")
				T model = (T) repo.makeFromEntityForType(
						(Class<? extends IJSONDocument>) Class.forName(entity
								.getType()), entity);
				
				if (cacheModels == true) {
					_modelsCache.put(position, model);
				}
				return model;
			} catch (InstantiationException e) {
				Log.e(TAG,
						"Error while creating from entity: " + e.getMessage());
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				Log.e(TAG,
						"Error while creating from entity: " + e.getMessage());
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				Log.e(TAG,
						"Error while creating from entity: " + e.getMessage());
				e.printStackTrace();
			}
		}

		return null;
	}

	@Override
	public void notifyDataSetChanged() {
		_modelsCache.clear();
		cursor = null;
		super.notifyDataSetChanged();
	}

	@Override
	public void notifyDataSetInvalidated() {
		_modelsCache.clear();
		cursor = null;
		super.notifyDataSetInvalidated();
	}

	private JSONEntity getEntityFromCursor(Cursor c, int position) {

		if (c.getCount() > 0) {
			int col_id = c.getColumnIndex("json_uid");
			int col_createDate = c.getColumnIndex("createDate");
			int col_updateDate = c.getColumnIndex("updateDate");
			int col_data = c.getColumnIndex("data");
			int col_type = c.getColumnIndex("type");

			c.moveToPosition(position);

			try {
				JSONEntity entity = new JSONEntity();
				entity.setUid(c.getInt(col_id));
				entity.setCreationDate(Util.ParseDateFromString(c
						.getString(col_createDate)));
				entity.setUpdateDate(Util.ParseDateFromString(c
						.getString(col_updateDate)));
				entity.setData(new JSONObject(c.getString(col_data)));
				entity.setType(c.getString(col_type));
				
				repo.getDb().getTagsForJSONEntity(entity, repo.getDb().getSQLDb());
				repo.getDb().getHasManyRelationsForJSONEntity(entity, repo.getDb().getSQLDb());
				repo.getDb().getBelongsToRelationsForJSONEntity(entity, repo.getDb().getSQLDb());
				
				return entity;
			} catch (JSONException e) {
				Log.d(TAG,
						"Error while getting entity from cursor: "
								+ e.getMessage());
				e.printStackTrace();
			}
		}

		return null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	// private class ViewHolder {
	//
	// public TextView title;
	// public TextView subtitle;
	//
	// }

	@Override
	public abstract View getView(int position, View v, ViewGroup parent);

	// protected abstract View initCellView(View v, Context context2);

	public SearchFields getQuery() {
		return query;
	}

	/**
	 * Query for this list
	 * 
	 * @param query
	 *            {@link SearchFields} Note that filter by type is already
	 *            included
	 */
	public void setQuery(SearchFields query) {
		this.query = query;
	}

	public int getModelCacheMaxSize() {
		return modelCacheMaxSize;
	}

	public void setModelCacheMaxSize(int modelCacheMaxSize) {
		this.modelCacheMaxSize = modelCacheMaxSize;
	}

	public boolean isLimitModelCache() {
		return limitModelCacheSize;
	}

	public void setLimitModelCache(boolean limitModelCache) {
		this.limitModelCacheSize = limitModelCache;
	}

	public boolean isCacheModels() {
		return cacheModels;
	}

	public void setCacheModels(boolean cacheModels) {
		this.cacheModels = cacheModels;
	}

}
