package net.smart_json_database.odm.annotations;

import net.smart_json_database.odm.IJSONDocument;

public @interface HasMany {

	Class<? extends IJSONDocument> value();

}
