package co.appfellas.android.smartjsonexample;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;

import net.smart_json_database.InitJSONDatabaseException;
import net.smart_json_database.JSONDatabase;
import net.smart_json_database.odm.Repository;
import net.smart_json_database.odm.SmartJsonList;
import net.smart_json_database.odm.adapters.SmartJsonCursorAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import co.appfellas.android.smartjsonexample.models.Playlist;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class PlaylistsActivity extends Activity {

	private static final String TAG = null;
	private ListView listView;
	private SmartJsonCursorAdapter<Playlist> adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.playlists);

		// seedTestData();
		// cleanDb();

		listView = (ListView) findViewById(R.id.playlist_list);

		adapter = new SmartJsonCursorAdapter<Playlist>(this, Playlist.class) {

			class ViewHolder {

				public TextView title;
				public TextView subtitle;
				public ImageView thumb;

			}

			public View getView(int position, View v, ViewGroup parent) {
				ViewHolder vh;

				if (v == null) {

					// v = initCellView(v, context);
					LayoutInflater li = (LayoutInflater) context
							.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					v = li.inflate(R.layout.playlist_cell, parent, false);

					vh = new ViewHolder();
					vh.title = (TextView) v.findViewById(R.id.cell_title);
					vh.subtitle = (TextView) v.findViewById(R.id.cell_subtitle);
					vh.thumb = (ImageView) v.findViewById(R.id.cell_thumb);
					v.setTag(vh);
				} else {
					vh = (ViewHolder) v.getTag();
				}

				Playlist item = getItem(position);
				if (item != null) {
					vh.title.setText(item.getName());
					if (item.getAuthor() != null) {
						vh.subtitle.setText(item.getAuthor().getName());
					} else {
						vh.subtitle.setText("No Author");
					}
					vh.thumb.setImageResource(R.drawable.kobza);
				}

				return v;
			}

		};

		listView.setAdapter(adapter);

	}

	private void cleanDb() {
		JSONDatabase db = getDb();
		if (db != null) {
			db.resetDb();
		}
		adapter.notifyDataSetChanged();
	}

	private JSONDatabase getDb() {

		Log.d(TAG, "Deleting DB");
		JSONDatabase db = null;
		try {
			db = JSONDatabase.GetDatabase(this);

		} catch (InitJSONDatabaseException e) {
			Log.e(TAG, "Error opening database: " + e.getMessage());
			e.printStackTrace();
		}

		return db;
	}

	@SuppressWarnings("unused")
	private void seedTestData() {
		Repository<Playlist> repo = new Repository<Playlist>(Playlist.class,
				this);

		int num = 500;
		ArrayList<Playlist> batch = new ArrayList<Playlist>();
		for (int i = 0; i < num; i++) {
			Playlist pl = repo.create();
			pl.setId(i * 2);
			pl.setAverageRating(20.0f);
			pl.setLiked(true);
			pl.setMix("Mix " + i);
			pl.setPrice(200.50);
			pl.setSubscribers(1000);
			pl.setTrack("Track name " + i);
			batch.add(pl);
		}

		long timeStamp = Calendar.getInstance().getTimeInMillis();
		repo.saveAll(batch, JSONDatabase.STRATEGY_REPLACE);
		long timeStampNew = Calendar.getInstance().getTimeInMillis();
		Log.d(TAG, "Saved batch of " + num + " objects in: "
				+ (double) ((double) timeStampNew - (double) timeStamp) / 1000
				+ " seconds");

	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		menu.add("Clear DB").setIcon(android.R.drawable.ic_menu_delete)
				.setOnMenuItemClickListener(new OnMenuItemClickListener() {

					@Override
					public boolean onMenuItemClick(MenuItem item) {
						cleanDb();
						return true;
					}
				}).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

		menu.add("Import data").setIcon(android.R.drawable.ic_menu_upload)
				.setOnMenuItemClickListener(new OnMenuItemClickListener() {

					@Override
					public boolean onMenuItemClick(MenuItem item) {
						importData();
						return true;
					}
				}).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

		getMenuInflater().inflate(R.menu.playlists, menu);
		return true;
	}

	protected void importData() {
		Log.d(TAG, "Importing data");

		String playlistJSON = null;
		String playlist133JSON = null;
		try {
			InputStream playlistsJSONIS = getResources().getAssets().open(
					"playlists.json");
			playlistJSON = inputStreamToString(playlistsJSONIS);

			InputStream playlist133JSONIS = getResources().getAssets().open(
					"playlist_133.json");
			playlist133JSON = inputStreamToString(playlist133JSONIS);

		} catch (IOException e) {
			Log.e(TAG, "Error reading a file: " + e.getMessage());
			e.printStackTrace();
		}

		if (playlist133JSON == null && playlistJSON == null) {
			Log.d(TAG, "Couldn't open files. Stopping here");
			return;
		}

		JSONDatabase db = getDb();
		if (db == null) {
			return;
		}

		ObjectMapper om = new ObjectMapper();

		try {
			SmartJsonList<Playlist> playlists = om.readValue(playlistJSON,
					new TypeReference<SmartJsonList<Playlist>>() {
					});

			Log.d(TAG, "Parsed " + playlists.size() + " objects");

			Repository<Playlist> repo = new Repository<Playlist>(
					Playlist.class, this);

			ArrayList<Integer> res = repo.saveAll(playlists, JSONDatabase.STRATEGY_UPDATE);
			if (res.size() > 0) {
				adapter.notifyDataSetChanged();
			} else {
				Log.e(TAG, "Error while saving items: " + res);
			}

		} catch (JsonParseException e) {
			Log.e(TAG, "Error while parsing response: " + e.getMessage());
			e.printStackTrace();
		} catch (JsonMappingException e) {
			Log.e(TAG, "Error while parsing response: " + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			Log.e(TAG, "Error while parsing response: " + e.getMessage());
			e.printStackTrace();
		}

	}

	/**
	 * Converts input stream to string
	 * 
	 * @param is
	 * @return String
	 * @throws IOException
	 */
	public static String inputStreamToString(InputStream is) throws IOException {
		InputStream in = new BufferedInputStream(is);

		BufferedReader r = new BufferedReader(new InputStreamReader(in));
		StringBuilder total = new StringBuilder();
		String line;
		while ((line = r.readLine()) != null) {
			total.append(line);
		}

		return total.toString();
	}

}
