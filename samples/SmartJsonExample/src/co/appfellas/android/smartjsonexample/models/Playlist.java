package co.appfellas.android.smartjsonexample.models;

import java.util.ArrayList;

import net.smart_json_database.odm.JSONDocument;
import net.smart_json_database.odm.annotations.BelongsTo;
import net.smart_json_database.odm.annotations.HasAndBelongsToMany;
import net.smart_json_database.odm.annotations.Id;
import net.smart_json_database.odm.annotations.IgnoreField;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Playlist extends JSONDocument {

	@IgnoreField
	@JsonIgnore
	private static final String TAG = "Playlist";

	@JsonDeserialize(as = User.class)
	@JsonProperty("creator")
	@BelongsTo(value = User.class)
	private User author;

	public float averageRating;
	@JsonProperty("created_at")
	private String createdAt;
	@Id
	public int id;
	public boolean liked;
	private String mix;
	private double price;

	@JsonProperty("song_count")
	private int songCount;

	@JsonProperty("songs")
	@HasAndBelongsToMany(Song.class)
	private ArrayList<Song> songs;

	@JsonProperty("subscribers")
	private int subscribers;

	@JsonProperty("track")
	private String track;

	@JsonProperty("name")
	private String name;

	public Playlist() {
		super();
	}

	public User getAuthor() {
		return author;
	}

	public float getAverageRating() {
		return averageRating;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public int getId() {
		return id;
	}

	public String getMix() {
		return mix;
	}

	public double getPrice() {
		return price;
	}

	public int getSongCount() {
		return songCount;
	}

	public ArrayList<Song> getSongs() {
		return songs;
	}

	public int getSubscribers() {
		return subscribers;
	}

	public String getTrack() {
		return track;
	}

	public boolean isLiked() {
		return liked;
	}

	public void setAuthor(User author) {
		this.author = author;
	}

	public void setAverageRating(float averageRating) {
		this.averageRating = averageRating;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setLiked(boolean liked) {
		this.liked = liked;
	}

	public void setMix(String mix) {
		this.mix = mix;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public void setSongCount(int songCount) {
		this.songCount = songCount;
	}

	public void setSongs(ArrayList<Song> songs) {
		this.songs = songs;
	}

	public void setSubscribers(int subscribers) {
		this.subscribers = subscribers;
	}

	public void setTrack(String track) {
		this.track = track;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
