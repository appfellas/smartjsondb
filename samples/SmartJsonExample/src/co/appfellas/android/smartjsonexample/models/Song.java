package co.appfellas.android.smartjsonexample.models;

import java.util.ArrayList;

import net.smart_json_database.odm.JSONDocument;
import net.smart_json_database.odm.annotations.HasAndBelongsToMany;
import net.smart_json_database.odm.annotations.Id;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonIgnoreProperties(ignoreUnknown = true)

public class Song extends JSONDocument {

	// "id": 63164,
	// "artist": "Niko Schwind",
	// "bitrate": 320,
	// "bpm": 124,
	// "created_at": "2011-10-16T18:24:20+02:00",
	// "file_hash":
	// "30ba49d1db89e3a8c8cf2a92a9215f6442ab65e7bea64db079dd281c62dcfada",
	// "image":
	// "http://revibe-albumart.s3.amazonaws.com/niko_schwind_-_just_a_groove",
	// "length": 449,
	// "likes_count": 11,
	// "mix": "Original Mix",
	// "plays_count": 468,
	// "track": "Just A Groove",
	// "updated_at": "2012-05-22T19:07:46+02:00",
	// "liked": true,
	// "playlist_song_id": "591"

	@JsonProperty("analyzed")
	private boolean analyzed;

	@JsonProperty("artist")
	private String artist;

	private boolean beatmatch;

	private int bitrate;
	private int bpm;

	@JsonProperty("country_code")
	private String countryCode;
	@JsonProperty("created_at")
	private String createdAt;

	private boolean disabled;
	
	@JsonIgnore
	@HasAndBelongsToMany(Playlist.class)
	private ArrayList<Playlist> playlists;

	// private String cuepoint;

	private String disk;

	@JsonProperty("file_hash")
	private String fileHash;
	
	@Id
	@JsonProperty("id")
	private int id;

	private String image;

	@JsonProperty("image_updated_at")
	private String imageUpdatedAt;

	private int length;

	private boolean liked;

	@JsonProperty("likes_count")
	private int likesCount;

	private String mix;

	@JsonIgnore
	private boolean playing = false;

	@JsonIgnore
	private long playlistId;

	@JsonProperty("plays_count")
	private int playsCount;

	private String track;

	@JsonProperty("updated_at")
	private String updatedAt;

	private String year;

	public String getArtist() {
		return artist;
	}

	public int getBitrate() {
		return bitrate;
	}

	public int getBpm() {
		return bpm;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public String getDisk() {
		return disk;
	}

	public String getFileHash() {
		return fileHash;
	}

	public int getId() {
		return id;
	}

	public String getImage() {
		return image;
	}

	public String getImageUpdatedAt() {
		return imageUpdatedAt;
	}

	public int getLength() {
		return length;
	}

	public int getLikesCount() {
		return likesCount;
	}

	public String getMix() {
		return mix;
	}

	public long getPlaylistId() {
		return playlistId;
	}

	public int getPlaysCount() {
		return playsCount;
	}

	public String getTrack() {
		return track;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public String getYear() {
		return year;
	}

	public boolean isAnalyzed() {
		return analyzed;
	}

	public boolean isBeatmatch() {
		return beatmatch;
	}

	public boolean isDisabled() {
		return disabled;
	}

	public boolean isLiked() {
		return liked;
	}

	public boolean isPlaying() {
		return playing;
	}

	public void setAnalyzed(boolean analyzed) {
		this.analyzed = analyzed;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public void setBeatmatch(boolean beatmatch) {
		this.beatmatch = beatmatch;
	}

	public void setBitrate(int bitrate) {
		this.bitrate = bitrate;
	}

	public void setBpm(int bpm) {
		this.bpm = bpm;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	public void setDisk(String disk) {
		this.disk = disk;
	}

	public void setFileHash(String fileHash) {
		this.fileHash = fileHash;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public void setImageUpdatedAt(String imageUpdatedAt) {
		this.imageUpdatedAt = imageUpdatedAt;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public void setLiked(boolean liked) {
		this.liked = liked;
	}

	public void setLikesCount(int likesCount) {
		this.likesCount = likesCount;
	}

	public void setMix(String mix) {
		this.mix = mix;
	}

	public void setPlaying(boolean playing) {
		this.playing = playing;
	}

	public void setPlaylistId(long playlistId) {
		this.playlistId = playlistId;
	}

	public void setPlaysCount(int playsCount) {
		this.playsCount = playsCount;
	}

	public void setTrack(String track) {
		this.track = track;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public ArrayList<Playlist> getPlaylists() {
		return playlists;
	}

	public void setPlaylists(ArrayList<Playlist> playlists) {
		this.playlists = playlists;
	}

}
