package co.appfellas.android.smartjsonexample.models;

import java.util.ArrayList;

import net.smart_json_database.odm.JSONDocument;
import net.smart_json_database.odm.annotations.HasMany;
import net.smart_json_database.odm.annotations.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class User extends JSONDocument {

	@Id
	@JsonProperty("id")
	private int id;

	@JsonProperty("display_name")
	private String name;

	@JsonProperty("fb_uid")
	private String facebookId;
	
	@HasMany(Playlist.class)
	private ArrayList<Playlist> playlists;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFacebookId() {
		return facebookId;
	}

	public void setFacebookId(String facebookId) {
		this.facebookId = facebookId;
	}

}
