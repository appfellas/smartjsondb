package co.appfellas.android.smartjsonexample;

import android.annotation.TargetApi;
import android.app.Application;
import android.os.Build;
import android.util.Log;

public class SmartJsonExampleApp extends Application {

	private static final String TAG = "SmartJsonExampleApp";

	@Override
	public void onCreate() {
		Log.i(TAG, "App Started");
		super.onCreate();
	}

	@Override
	public void onLowMemory() {
		Log.e(TAG, "Low Memory!!!");
		super.onLowMemory();
	}

	@Override
	public void onTerminate() {
		Log.w(TAG, "Terminate app");
		super.onTerminate();
	}

	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	@Override
	public void onTrimMemory(int level) {
		Log.d(TAG, "On Trim Memory: " + level);
	}
}
