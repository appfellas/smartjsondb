package co.appfellas.android.smartjsonexample.tests;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;

import net.smart_json_database.JSONDatabase;
import net.smart_json_database.JSONEntity;
import net.smart_json_database.SearchFields;
import net.smart_json_database.odm.Repository;
import net.smart_json_database.odm.SmartJsonList;
import net.smart_json_database.odm.exceptions.ModelIdIsNotDefinedException;

import org.json.JSONException;

import android.content.Context;
import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;
import co.appfellas.android.smartjsonexample.PlaylistsActivity;
import co.appfellas.android.smartjsonexample.models.Playlist;
import co.appfellas.android.smartjsonexample.models.Song;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class EntityModelTestSuite extends
		ActivityInstrumentationTestCase2<PlaylistsActivity> {

	public EntityModelTestSuite() {
		super(PlaylistsActivity.class);
	}

	public EntityModelTestSuite(Class<PlaylistsActivity> activityClass) {
		super(activityClass);
	}

	private JSONDatabase db;
	protected String TAG = "EntityModelTestSuite";
	private Context context;

	public void setUp() throws Exception {
		context = this.getInstrumentation().getContext();
		db = JSONDatabase.GetDatabase(context);
		db.resetDb();
	}

	public void tearDown() throws Exception {
		db.resetDb();
	}

	private String getPlaylistDetailsData() {
		String json = null;
		try {

			InputStream playlist133JSONIS = context.getResources().getAssets()
					.open("playlist_133_5_songs.json");
			json = PlaylistsActivity
					.inputStreamToString(playlist133JSONIS);

		} catch (IOException e) {
			Log.e(TAG, "Error reading a file: " + e.getMessage());
			e.printStackTrace();
		}
		//Log.d(TAG, json);

		return json;
	}

	private String getPlaylistData() {

		String playlistJSON = null;

		try {
			InputStream playlistsJSONIS = context.getResources().getAssets()
					.open("playlists.json");
			playlistJSON = PlaylistsActivity
					.inputStreamToString(playlistsJSONIS);

		} catch (IOException e) {
			Log.e(TAG, "Error reading a file: " + e.getMessage());
			e.printStackTrace();
		}
		Log.d(TAG, playlistJSON);

		return playlistJSON;
	}

	private Playlist[] mapPlaylists(String json) {

		ObjectMapper om = new ObjectMapper();
		Playlist[] playlists = null;
		try {
			playlists = om.readValue(json,
					new TypeReference<Playlist[]>() {
					});

			Log.d(TAG, "Parsed " + playlists.length + " objects");

		} catch (JsonParseException e) {
			Log.e(TAG, "Error while parsing response: " + e.getMessage());
			e.printStackTrace();
		} catch (JsonMappingException e) {
			Log.e(TAG, "Error while parsing response: " + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			Log.e(TAG, "Error while parsing response: " + e.getMessage());
			e.printStackTrace();
		}

		return playlists;
	}

	private Playlist mapPlaylist(String json) {
		ObjectMapper om = new ObjectMapper();
		Playlist playlist = null;
		try {
			playlist = om.readValue(json,
					new TypeReference<Playlist>() {
					});

			Log.d(TAG, "Parsed " + playlist.getName() + " playlist");
			Log.d(TAG, "It has " + playlist.getSongs() + " songs");

		} catch (JsonParseException e) {
			Log.e(TAG, "Error while parsing response: " + e.getMessage());
			e.printStackTrace();
		} catch (JsonMappingException e) {
			Log.e(TAG, "Error while parsing response: " + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			Log.e(TAG, "Error while parsing response: " + e.getMessage());
			e.printStackTrace();
		}

		return playlist;
	}

	public void testShouldRetrieveAllEntries() throws Exception {
		Repository<Playlist> repo = new Repository<Playlist>(Playlist.class,
				context);

		SmartJsonList<Playlist> playlists = repo.all();

		assertNotNull(playlists);
		assertEquals(new SmartJsonList<Playlist>(), playlists);

		assertEquals(0, playlists.size());

		// Add one item
		JSONEntity entity = new JSONEntity(Playlist.class.getName());
		entity.put("track", "Some title");
		entity.put("mix", "A mix");
		entity.put("subscribers", 20);
		entity.put("averageRating", 20.0f);
		entity.put("liked", true);
		entity.put("price", 50.0);

		int storeRes = db.store(entity);
		assertNotSame(JSONDatabase.OPERATION_STATUS_ERROR, storeRes);

		playlists = repo.all();

		assertEquals(1, playlists.size());
		assertEquals("Some title", playlists.first().getTrack());
		assertEquals("A mix", playlists.first().getMix());
		assertEquals(20, playlists.first().getSubscribers());
		assertEquals(20.0f, playlists.first().getAverageRating());
		assertEquals(true, playlists.first().isLiked());
		assertEquals(50.0, playlists.first().getPrice());

		// Add an item
		entity = new JSONEntity(Playlist.class.getName());
		entity.put("track", "Some title 2");
		entity.put("mix", "A mix 2");
		entity.put("subscribers", 30);
		entity.put("averageRating", 30.5f);
		entity.put("liked", false);
		entity.put("price", 75.2);

		storeRes = db.store(entity);
		assertNotSame(JSONDatabase.OPERATION_STATUS_ERROR, storeRes);

		// Check 2 items

		playlists = repo.all();

		assertEquals(2, playlists.size());
		assertEquals("Some title", playlists.first().getTrack());
		assertEquals("A mix", playlists.first().getMix());

		assertEquals("Some title 2", playlists.get(1).getTrack());
		assertEquals("A mix 2", playlists.get(1).getMix());
		assertEquals(30, playlists.get(1).getSubscribers());
		assertEquals(30.5f, playlists.get(1).getAverageRating());
		assertEquals(false, playlists.get(1).isLiked());
		assertEquals(75.2, playlists.get(1).getPrice());

	}

	public void testShouldCreateNewEntry() throws Exception {
		Repository<Playlist> repo = new Repository<Playlist>(Playlist.class,
				context);

		Playlist pl = repo.create();
		pl.setId(1);
		Log.d(TAG, pl.toString());
		assertNotNull(pl);

		boolean ret = pl.save();
		assertNotNull(ret);
		assertTrue(ret);

		SmartJsonList<Playlist> all = repo.all();
		assertEquals(1, all.size());

		Playlist pl2 = repo.create();
		pl2.setId(2);

		assertNotNull(pl2);
		ret = pl2.save();
		assertNotNull(ret);
		assertTrue(ret);

		all = repo.all();
		assertEquals(2, all.size());

		ret = pl2.save(); // Should update but not create new one
		assertNotNull(ret);
		assertTrue(ret);

		all = repo.all();
		assertEquals(2, all.size());

	}

	// XXX Fix this
	public void testShouldReturnSortedResults() {
		// Repository<Playlist> repo = new Repository<Playlist>(Playlist.class,
		// context);

		// repo.all().sortBy("title", Repository.ASC);
	}

	public void testPlaingWithModelAPI() {

		Repository<Playlist> model = new Repository<Playlist>(Playlist.class,
				context);

		SmartJsonList<Playlist> playlists = model.all();
		assertNotNull(playlists);
		assertEquals(new SmartJsonList<Playlist>(), playlists);

		Playlist newPl = model.create();
		assertNotNull(newPl);
		newPl.setId(100);
		try {
			newPl.save();
		} catch (ModelIdIsNotDefinedException e1) {
			fail("Model must have an id");
			e1.printStackTrace();
		}

		int id = newPl.get_superId();
		assertNotNull(id);
		JSONEntity entity = db.fetchById(id);

		assertNotNull(entity);
		assertEquals(newPl.get_superId(), entity.getUid());

		// Add some values
		newPl.setId(123);
		newPl.setTrack("Some Track");
		newPl.setMix("Radio Mix");
		newPl.setSubscribers(20);
		newPl.setAverageRating(20.0f);
		newPl.setLiked(true);
		newPl.setPrice(50.0);

		boolean res = false;
		try {
			res = newPl.save();
		} catch (ModelIdIsNotDefinedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		assertTrue(res);

		playlists = model.all();
		assertEquals(1, playlists.size());

		Playlist pl = playlists.first();
		assertNotNull(pl);
		Log.d(TAG, "Got playlist: " + pl.toString());
		assertEquals("Some Track", pl.getTrack());
		assertEquals("Radio Mix", pl.getMix());
		assertEquals(20, pl.getSubscribers());
		assertEquals(20.0f, pl.getAverageRating());
		assertTrue(pl.isLiked());
		assertEquals(50.0, pl.getPrice());
		assertEquals(123, pl.getId());

		int count = model.all().count();
		assertEquals(1, count);

		playlists = model.where(SearchFields.Where("track", "Some Mix").Or(
				"mix", "Radio Mix"));

		assertEquals(1, playlists.size());
		Log.d(TAG, "Playlist: " + playlists.first().toString());

		JSONEntity playlist = db.fetchById(playlists.first().get_superId());

		Log.d(TAG, "Playlist JSONEntity: " + playlist);

		try {
			assertEquals(123, playlist.getInt("id"));
			assertEquals("Some Track", playlist.getString("track"));
			assertEquals("Radio Mix", playlist.getString("mix"));
			assertEquals(20, playlist.getInt("subscribers"));
			assertEquals(20.0f, (float) playlist.getDouble("averageRating"));
			assertTrue(playlist.getBoolean("liked"));
			assertEquals(50.0, playlist.getDouble("price"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void testShouldFindDocumentById() throws Exception {
		Repository<Playlist> model = new Repository<Playlist>(Playlist.class,
				context);

		Playlist pl = model.create();
		pl.setId(10);
		pl.save();

		Playlist pl2 = model.create();
		pl2.setId(15);
		pl2.save();

		Playlist pl3 = model.create();
		pl3.setId(20);
		pl3.save();

		Playlist playlist = model.find(pl.getId());

		assertNotNull(playlist);
		assertEquals(pl.getId(), playlist.getId());

		playlist = model.find(pl2.getId());

		assertNotNull(playlist);
		assertEquals(pl2.getId(), playlist.getId());

		playlist = model.find(pl3.getId());

		assertNotNull(playlist);
		assertEquals(pl3.getId(), playlist.getId());

		playlist = model.find(100000); // Not existing returns null
		assertNull(playlist);

	}

	public void testShouldReturnLimitedSet() {
		Repository<Playlist> model = new Repository<Playlist>(Playlist.class,
				context);
		long timeStamp = Calendar.getInstance().getTimeInMillis();
		int num = 25;
		for (int i = 0; i < num; i++) {
			Playlist pl = model.create();
			pl.setId(i + 1 * 2); // Id cannot be 0
			pl.setAverageRating(20.0f);
			pl.setLiked(true);
			pl.setMix("Mix " + i);
			pl.setPrice(200.50);
			pl.setSubscribers(1000);
			pl.setTrack("Track name " + i);
			try {
				pl.save();
			} catch (ModelIdIsNotDefinedException e) {
				fail("Id must be set up");
				e.printStackTrace();
			}
		}
		long timeStampNew = Calendar.getInstance().getTimeInMillis();
		Log.d(TAG, "Created " + num + " objects in: "
				+ (double) ((double) timeStampNew - (double) timeStamp) / 1000
				+ " seconds");

		timeStamp = Calendar.getInstance().getTimeInMillis();
		SmartJsonList<Playlist> all = model.all();
		timeStampNew = Calendar.getInstance().getTimeInMillis();
		Log.d(TAG, "Retrieved " + num + " objects from db in: "
				+ (double) ((double) timeStampNew - (double) timeStamp) / 1000
				+ " seconds");

		assertEquals(num, all.size());

	}

	public void testToJsonEntity() throws Exception {

		Repository<Playlist> model = new Repository<Playlist>(Playlist.class,
				context);

		Playlist pl = model.create();
		pl.setId(12);
		pl.setLiked(true);
		pl.setAverageRating(20.0f);
		pl.setMix("Mix");
		pl.setPrice(10.0);

		JSONEntity entity = model._toJSONEntity(pl);
		assertNotNull(entity);
		assertEquals(12, entity.getInt("id"));
		assertEquals(true, entity.getBoolean("liked"));
		assertEquals(20.0f, (float) entity.getDouble("averageRating"));
		assertEquals("Mix", entity.getString("mix"));
		assertEquals(10.0, entity.getDouble("price"));
		assertEquals(pl.getClass().getName(), entity.getType());

		// Log.d(TAG, entity.toString());
	}

	public void testEntitiesToModelsConversion() throws Exception {
		Repository<Playlist> model = new Repository<Playlist>(Playlist.class,
				context);

		Playlist pl1 = model.create();
		pl1.setId(123);
		pl1.save();

		Collection<JSONEntity> entities = db.fetchByType(pl1.getClass()
				.getName());
		assertNotNull(entities);
		assertEquals(1, entities.size());

		SmartJsonList<Playlist> playlists = model.entitiesToModels(entities);
		assertNotNull(playlists);
		assertEquals(1, playlists.size());
		assertEquals(123, playlists.first().getId());
	}

	public void testShouldSaveBatchOfItems() throws Exception {
		Repository<Playlist> model = new Repository<Playlist>(Playlist.class,
				context);

		int num = 25;
		ArrayList<Playlist> batch = new ArrayList<Playlist>();
		for (int i = 0; i < num; i++) {
			Playlist pl = model.create();
			pl.setId(i * 2);
			pl.setAverageRating(20.0f);
			pl.setLiked(true);
			pl.setMix("Mix " + i);
			pl.setPrice(200.50);
			pl.setSubscribers(1000);
			pl.setTrack("Track name " + i);
			batch.add(pl);
		}

		long timeStamp = Calendar.getInstance().getTimeInMillis();
		ArrayList<Integer> ret = model.saveAll(batch, JSONDatabase.STRATEGY_REPLACE);
		long timeStampNew = Calendar.getInstance().getTimeInMillis();
		Log.d(TAG, "Saved batch of " + num + " objects in: "
				+ (double) ((double) timeStampNew - (double) timeStamp) / 1000
				+ " seconds");

		assertEquals(batch.size(), ret.size());

		SmartJsonList<Playlist> playlists = model.all();
		assertEquals(num, playlists.size());

		for (Playlist playlist : playlists) {
			assertNotSame(-1, playlist.get_superId());
			Log.d(TAG, "Playlist superId: " + playlist.get_superId());
		}

		for (Playlist playlist : playlists) {
			playlist.setTrack("Changed Track: " + playlist.getId());
		}

		Playlist pl = model.create();
		pl.setId(220);

		playlists.add(pl);

		timeStamp = Calendar.getInstance().getTimeInMillis();

		ret = model.saveAll(playlists, JSONDatabase.STRATEGY_REPLACE);
		timeStampNew = Calendar.getInstance().getTimeInMillis();
		Log.d(TAG, "Saved batch of " + num + 1 + " existing objects in: "
				+ (double) ((double) timeStampNew - (double) timeStamp) / 1000
				+ " seconds");
		assertEquals(playlists.size(), ret.size());

		playlists = model.all();
		assertEquals(num + 1, playlists.size());

	}

	public void testShouldSaveWithRelations() throws Exception {

		String playlistJSON = getPlaylistData();

		assertNotNull(playlistJSON);

		Playlist[] playlists = mapPlaylists(playlistJSON);

		assertNotNull(playlists);

		Repository<Playlist> repo = new Repository<Playlist>(Playlist.class,
				context);

		ArrayList<Integer> res = repo.saveAll(playlists);
		assertEquals(playlists.length, res.size());

		SmartJsonList<Playlist> playlistsAll = repo.all();

		assertEquals(playlists.length, playlistsAll.size());

		for (int i = 0; i < playlistsAll.size(); i++) {
			assertNotNull(playlistsAll.get(i).getAuthor());
			assertEquals(playlists[i].getAuthor().getId(), playlistsAll.get(i)
					.getAuthor().getId());
			assertEquals(playlists[i].getAuthor().getName(), playlistsAll
					.get(i).getAuthor().getName());
			assertEquals(playlists[i].getAuthor().getFacebookId(), playlistsAll
					.get(i).getAuthor().getFacebookId());
		}

	}

	public void testShouldSaveHasManyRelation() throws Exception {

		String playlist133JSON = getPlaylistDetailsData();
		assertNotNull(playlist133JSON);

		Playlist playlist = mapPlaylist(playlist133JSON);
		assertNotNull(playlist);

		Repository<Playlist> repo = new Repository<Playlist>(Playlist.class,
				context);

		boolean res = repo.save(playlist);
		assertTrue(res);
		
		
		SmartJsonList<Playlist> playlists = repo.all();
		
		assertEquals(1, playlists.size());
		assertEquals(playlist.getSongs().size(), playlists.get(0).getSongs().size());
		
		
		Repository<Song> songRepo = new Repository<Song>(Song.class,
				context);
		
		SmartJsonList<Song> songs = songRepo.all();
		
		assertEquals(playlist.getSongs().size(), songs.size());
		
		for (int i=0; i < songs.size(); i++) {
			assertEquals(playlist.getSongs().get(i).getArtist(), songs.get(i).getArtist());
			assertEquals(playlist.getSongs().get(i).getBitrate(), songs.get(i).getBitrate());
			assertEquals(playlist.getSongs().get(i).getBpm(), songs.get(i).getBpm());
			assertEquals(playlist.getSongs().get(i).getId(), songs.get(i).getId());
			assertEquals(playlist.getSongs().get(i).getCountryCode(), songs.get(i).getCountryCode());
			assertEquals(playlist.getSongs().get(i).getCreatedAt(), songs.get(i).getCreatedAt());
			assertEquals(playlist.getSongs().get(i).getDisk(), songs.get(i).getDisk());
			assertEquals(playlist.getSongs().get(i).getFileHash(), songs.get(i).getFileHash());
			assertEquals(playlist.getSongs().get(i).getImage(), songs.get(i).getImage());
			assertEquals(playlist.getSongs().get(i).getImageUpdatedAt(), songs.get(i).getImageUpdatedAt());
			assertEquals(playlist.getSongs().get(i).getLength(), songs.get(i).getLength());
			assertEquals(playlist.getSongs().get(i).getLikesCount(), songs.get(i).getLikesCount());
			assertEquals(playlist.getSongs().get(i).getMix(), songs.get(i).getMix());
			//assertEquals(playlist.getSongs().get(i).getPlaylistId(), songs.get(i).getPlaylistId());
			assertEquals(playlist.getSongs().get(i).getTrack(), songs.get(i).getTrack());
			
		}
		

	}

	public void testModelShouldHaveIdFieldSetBeforeSave() throws Exception {

		Repository<Playlist> repo = new Repository<Playlist>(Playlist.class,
				context);

		String message = null;
		try {
			Playlist model = repo.create();
			model.setId(10);
			model.save();
		} catch (ModelIdIsNotDefinedException e) {
			message = e.getMessage();
		}

		assertNull(message);

		message = null;
		try {
			Playlist model = repo.create();
			model.save();
		} catch (ModelIdIsNotDefinedException e) {
			message = e.getMessage();
		}

		assertNotNull(message);
		assertEquals("Model has not value for Id field", message);

	}

}
