package co.appfellas.android.smartjsonexample.tests;

import net.smart_json_database.IDatabaseChangeListener;
import net.smart_json_database.JSONDatabase;
import net.smart_json_database.JSONEntity;

import org.json.JSONException;

import android.content.Context;
import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;
import co.appfellas.android.smartjsonexample.PlaylistsActivity;

public class JSONDatabaseTest extends
		ActivityInstrumentationTestCase2<PlaylistsActivity> {
	
	public JSONDatabaseTest() {
		super(PlaylistsActivity.class);
	}

	public JSONDatabaseTest(Class<PlaylistsActivity> activityClass) {
		super(activityClass);
	}

	private JSONDatabase db;
	protected String TAG = "JSONDatabaseTest";

	public void setUp() throws Exception {
		Context context = this.getInstrumentation().getContext();
		db = JSONDatabase.GetDatabase(context);
		db.resetDb();
	}

	public void tearDown() throws Exception {
		db.resetDb();
	}

	
	public void testDBIsInstantiated() {
		assertNotNull(db);
	}

	
	public void testAddListener() {
		
		String type = "TestEntity";
		
		IDatabaseChangeListener listener = new IDatabaseChangeListener() {
			
			@Override
			public void onTagChange(String name, int _changeType) {
				Log.d(TAG , "On Tag Change: name: "+name+ ": Change Type: "+_changeType);
			}
			
			@Override
			public void onEntityChange(int _id, int _changeType) {
				Log.d(TAG , "On Entity Change: _id: "+_id+ ": Change Type: "+_changeType);
				//assertEquals(1, _id);
				//assertEquals(0, _changeType);
			}
		};
		db.addListener(listener);
		
		
		JSONEntity entity = new JSONEntity(type);
		try {
			entity.put("Test", "Test");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		int id = db.store(entity);
		assertNotSame(JSONDatabase.OPERATION_STATUS_ERROR, id);
		entity = db.fetchById(id);
		boolean deleteStatus = db.delete(entity);
		
		assertTrue(deleteStatus);
		
		JSONEntity res = db.fetchById(id);
		assertNull(res);
	}

	
	public void testRemoveListener() {
		// fail("Not yet implemented"); // TODO
	}

	
	public void testFetchById() {
		// fail("Not yet implemented"); // TODO
	}

	
	public void testFetchByTag() {
		// fail("Not yet implemented"); // TODO
	}

	
	public void testFetchByFields() {
		// fail("Not yet implemented"); // TODO
	}

	
	public void testFetchAllEntities() {
		// fail("Not yet implemented"); // TODO
	}

	
	public void testFetchByType() {
		// fail("Not yet implemented"); // TODO
	}

	
	public void testFetchManyByIds() {
		// fail("Not yet implemented"); // TODO
	}

	
	public void testStore() {
		// fail("Not yet implemented"); // TODO
	}

	
	public void testInsert() {
		// fail("Not yet implemented"); // TODO
	}

	
	public void testUpdate() {
		// fail("Not yet implemented"); // TODO
	}

	
	public void testDelete() {
		// fail("Not yet implemented"); // TODO
	}

	
	public void testGetTagNames() {
		// fail("Not yet implemented"); // TODO
	}

	
	public void testInsertTag() {
		// fail("Not yet implemented"); // TODO
	}

	
	public void testDeleteTag() {
		// fail("Not yet implemented"); // TODO
	}

	
	public void testClearAllTables() {
		// fail("Not yet implemented"); // TODO
	}

	
	public void testGetPropertyKeys() {
		// fail("Not yet implemented"); // TODO
	}

	
	public void testSetProperty() {
		// fail("Not yet implemented"); // TODO
	}

	
	public void testGetPropertyString() {
		// fail("Not yet implemented"); // TODO
	}

	
	public void testGetPropertyStringString() {
		// fail("Not yet implemented"); // TODO
	}

}
