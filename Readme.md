# SmartJSON DB

Note: This project is originally made by [https://code.google.com/p/smart-json-databsase](https://code.google.com/p/smart-json-databsase). I have merely corrected a few typos and migrated it to Git.

**Latest version: 1.0.4 BETA** 

[Download JAR 1.0.4 Beta - built against Android API 8](https://bitbucket.org/appfellas/smartjsondb/downloads/smart_json_database-BETA_1_4_0.jar)


## Usage:

### Create the JSONDatabaseConfiguration.xml

In the JSONDatabaseConfiguration.xml you configure the name of your database, the version and if you change the database version a upgrade-class.

You have to put this JSONDatabaseConfiguration.xml to your assets (if your Android-App-Project has no assets - create a assets folder) and the xml must have the name JSONDatabaseConfiguration.xml.

**Example:**
    
    :::xml
    <?xml version="1.0" encoding="UTF-8"?>
    <JSONDatabaseConfiguration> 
            <Database name="JSONDatabase.db" version="1" upgradeClass=""></Database>
    </JSONDatabaseConfiguration>


### Store and fetch entities

#### Fetch By Id
    
    :::java
    try {
            database = JSONDatabase.GetDatabase(this);
            JSONEntity book1 = new JSONEntity("Book");
            book1.put("Title", "Book 1");
            book1.put("Price", 0.50);
            int id = database.store(book1);

            JSONEntity book1_2 = database.fetchById(id);
        
            String title = book1_2.getString("Title");

    } catch (InitJSONDatabaseExcepiton e) {

    e.printStackTrace();
                
    } catch (JSONException e) {

    e.printStackTrace();
                        
    }
    

#### Fetch By Field

    :::java
    SearchFields search = SearchFields.Where("Title", "Book 1").Or("Name", "James");
    Collection<JSONEntity> entities = database.fetchByFields(search);
    
    
#### Fetch By Type

    :::java
    Collection<JSONEntity> entities = database.fetchByType("Book");
    

#### Make Relations

    :::java
    JSONEntity book1_2 = database.fetchById(1);
    JSONEntity author1 = new JSONEntity("Author");
    author1.put("Name", "James");
    int id = database.store(author1);
    book1_2.addIdToHasMany("writtenBy", id);
    int id2 = database.store(book1_2);
    JSONEntity author1_2 = database.fetchById(id);
    Collection<Integer> collection = author1_2.belongsTo("writtenBy");
    
    
## Building from Source

First you need to set environment variable "ANDROID_HOME" which should point to your Android SDK directory.

Then run in Terminal from the project directory
 
    :::bash
    ant clean dist
    
This will build JAR file in "dist/lib"

